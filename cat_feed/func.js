pic_clicked=(cat)=>{
    document.getElementById(cat).src = 'pic/'+cat+'_inverse.jpg';
    setTimeout(`document.getElementById('${cat}').src = 'pic/${cat}.jpg';`,200);
}

submit_clicked=()=>{
    //Specific cat
    for(var i=1;i<6;i++){                
        if (document.getElementById(`in_cat${i}`).checked){   
            document.getElementById(`cat${i}_feed`).style.zIndex = 2;
            document.getElementById(`cat${i}_feed`).src = 'pic/feed.png';
        }   
    }

    //All cats
    if(document.getElementById('in_cats').checked){
        for(var i=1;i<6;i++){                          
            document.getElementById(`cat${i}_feed`).style.zIndex = 2;
            document.getElementById(`cat${i}_feed`).src = 'pic/feed.png';
        }
    }

    //text
    for(var i=1;i<6;i++){                
        if (document.getElementById('in_text').value === document.getElementById(`in_cat${i}`).value){   
            document.getElementById(`cat${i}_feed`).style.zIndex = 2;
            document.getElementById(`cat${i}_feed`).src = 'pic/feed.png';
        }   
    }
}